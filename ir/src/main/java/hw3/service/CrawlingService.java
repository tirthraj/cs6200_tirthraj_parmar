/**
 * 
 */
package hw3.service;

import hw3.model.Frontier;
import hw3.model.WebPage;

/**
 * @author Tirthraj
 *
 */
public interface CrawlingService {
	public abstract WebPage getCrawledUrlPage(Frontier frontier);
}
