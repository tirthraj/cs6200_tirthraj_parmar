/**
 * 
 */
package hw3.service;

import java.util.ArrayList;

import hw3.model.Frontier;
import hw3.model.WebPage;

/**
 * @author Tirthraj
 *
 */
public interface QueueingService {
	public abstract ArrayList<Frontier> getFrontierList(int queuefetchsize);
	public abstract void enqueueOutLinks(WebPage webPage, String query);
	public abstract void removeExtraUrls(int maxQueueSize);
}
