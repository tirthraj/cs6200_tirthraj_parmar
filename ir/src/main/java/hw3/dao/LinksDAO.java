/**
 * 
 */
package hw3.dao;

import java.util.ArrayList;

import hw3.model.Links;

/**
 * @author Tirthraj
 *
 */
public interface LinksDAO {
	public abstract void storeNLinks(ArrayList<Links> linkList);
}
