/**
 * 
 */
package hw3.dao;

import org.jsoup.Connection.Response;

import hw3.model.WebPage;

public interface WebPageDAO {
	public abstract WebPage getWebPage(String url, int waveNo, Response response);
}
