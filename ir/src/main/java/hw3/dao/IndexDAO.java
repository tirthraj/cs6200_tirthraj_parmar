/**
 * 
 */
package hw3.dao;

import hw3.model.Index;

/**
 * @author Tirthraj
 *
 */
public interface IndexDAO {
	public abstract void storeIndex(Index index);
	public abstract long getIndexedDocumentsCount();
}
